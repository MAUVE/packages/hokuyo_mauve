/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Hokuyo project.
 *
 * MAUVE Hokuyo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Hokuyo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#include "hokuyo/mauve/Hokuyo.hpp"

using namespace mauve::runtime;
using namespace hokuyo::mauve;
using namespace mauve::types::sensor;

struct HokuyoArchitecture : public Architecture {
  Hokuyo & hokuyo = mk_component< Hokuyo >("hokuyo");
  SharedData<LaserScan> & scan =
    mk_resource< SharedData<LaserScan> >("scan", LaserScan());

  bool configure_hook() override {
    hokuyo.shell().scan.connect(scan.interface().write);
    hokuyo.fsm().period = ms_to_ns(100);
    hokuyo.configure();
    scan.configure();
    return true;
  }
};

int main() {
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  type: stdout" << std::endl;
  config << "  level: info" << std::endl;
  config << "hokuyo:" << std::endl;
  config << "  - type: stdout" << std::endl;
  config << "    level: trace" << std::endl;
  AbstractLogger::initialize(config);

  HokuyoArchitecture archi;
  AbstractDeployer* depl = mk_deployer(&archi);
  //AbstractDeployer* depl = AbstractDeployer::instance();
  archi.configure();
  depl->create_tasks();
  depl->activate();
  depl->start();

  depl->loop();

  depl->stop();
  depl->clear_tasks();
  archi.cleanup();

  return 0;
}
