#!/usr/bin/ipython -i
import mauve_runtime
mauve_runtime.load_deployer("devel/lib/libmauve_hokuyo_deployer.so")

mauve_runtime.initialize_logger(b'''
default:
  level: debug
  type: stdout
''')

deployer = mauve_runtime.deployer
deployer.architecture.configure()
deployer.create_tasks()
deployer.activate()
deployer.start()
